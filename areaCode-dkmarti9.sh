#Module 6 Lab

#56 Area Code Lookup

#!/bin/bash

#1. Take user input to select an area code.
#use read -p to prompt the user for an area code.
read -p "Enter an area code: " code

int=^[0-9]+$

#2. Check to make sure the user put numbers in as the area code.
#Compare the variable entered by the user to the regex representation of an interger ^[0-9]+$.
#Use =~ to do a regex comparison in an if statement. Put ! infront of the brackets to signify 
#we are looking for a false outcome. If the code variable isnt an int echo an error message and exit.
if  ! [[ $code =~ $int ]]
then
	echo "The area code must be a number"
	exit
	
#3. Now check to make sure the area code is 3 numbers long.
#use elif to start an else if statement. use ! to signify we are looking for cases that dont match the brackets.
#to do a command in an if statement put it in "$()
#to compare character length echo the variable and pipe it into a wc -c which will count the number of characters.
#the output should be 4 since there are 3 characters and 1 end of line character. set the command equal to
#4 with -eq 4. If it is not equal to 4 tell the user to input a value 3 characters long and exit.
elif ! [[ "$(echo $code | wc -c)" -eq 4 ]]
then
	echo "area code must be 3 numbers long"
	exit
else
#4. Now that the checks are out of the way search for the area code on the bennetyee.org website.
#Store the outcome of the curl command in a variable by setting a variable equal to $(command)
#curl the website and pipe it into a grep which will look for a case insensitive indicator starting with name="$code
#Pipe the outcome into a cut with the delimeter set to > and select the 11th field to remove the first half of the code.
#Pipe this into another cut with the delimiter set to ( and select the first field. to remove end useless code.
#Line starts with annoying triple space so use sed to remove it by replacing it with //
	info=$(curl -s -dump https://www.bennetyee.org/ucsd-pages/area.html | grep -i "name=\"$code" | cut -d '>' -f 11 \
	| cut -d "(" -f 1 | sed 's/   //g')

#5. Check to make sure the users area code exists and output message depending on outcome.
#Compare -z to the info variable in an if statement. -z checks to see if the variable is empty
#If the variable is empty the area code dosent exist so echo area code empty
#If the variable isnt empty echo the contents to the user.
	if [[ -z $info ]]
	then
		echo "This area code does not exist"
	else
		echo "Area code $code belongs to: $info"
	fi
fi
