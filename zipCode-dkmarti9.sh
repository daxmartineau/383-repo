#Module 6 Lab

#56 Zip Code Lookup

#!/bin/bash

#1. Take input from user to get ZIP code
#Use read -p to prompt user for zip code and store in variable
read -p "Insert a ZIP Code: " ZIP
int=^[0-9]+$

#2. Check to make sure the user put in a number as their zip code.
#compare the zip variable to ^[0-9]+$ which is the regex representation of an integer.
#to do regex comparisons in a if statement use =~
#if the input is not a number echo the reason for failure and exit
if  ! [[ $ZIP =~ $int ]]
then
	echo "The zipcode must be a number"
	exit

#Zip codes are 5 numbers long so check to make sure the zip code is 5 characters long. 
#take the word count of the zip variable with wc -c which will count characters.
#The variable will have a end of line character so it should be 6 characters long.
#use ! in front of the brackets to signify you are looking for cases where it isnt 6 characters long
#if zip isnt 5 numbers echo an error message and exit.
elif ! [[ "$(echo $ZIP | wc -c)" -eq 6 ]]
then
	echo "zip code must be 5 numbers long"
	exit
else

#3. Search the zip code webpage for the information on the zipcode the user input.
#There will be multiple values so save them all to one variable.
#To set a variable equal to a command, put the command in a $()
#use curl on the zip-codes.com webpage and replace both example zip codes with the zip variable
# -s will stop curl from outputting too much info and -dump will keep headers in the text.
#pass that input into a grep which will search for the <title> section of the code that houses relevant information.
#pass that into a cut with a delimeter of " " and select fields 2 and fields 4-10 for states longer than 1 word.
	info=$(curl  -s -dump https://www.zip-codes.com/zip-code/$ZIP/zip-code-$ZIP.asp | grep -i '<title>' | \
	cut -d " " -f 2,4-10)
#the info variable holds 3 values seperated by spaces. Cut the first value of info and save it as a variable
#Use that variable in an if statement. If the variable is empty then the search failed and the
#zip code doset exist.
#Compare check to -z which will return true if the variable is empty.
#If the check variable is empty then tell the user its a bad zip code and exit.
	check=$(echo $info | cut -d " " -f 1)
	if [[ -z $check ]]
	then
		echo "Invalid Zip Code"
		exit
	else	
#4. If the zip code is a real zip code, return the values for the city and state that zip code belongs too.
#The first value of info is a test so cut everything from 2-10. Pipe this into another cut that
#will remove everything after < to remove the <title> header at the end.
#pipe that into a sed command to replace the () around the state name with backspaces.
#cut the third value of the info variable and save it as the state variable.
		city=$(echo $info | cut -d " " -f 2-10 | cut -d '<' -f 1 | sed 's/[()]//g')
#create an echo statement that outputs the zipcode city variables.
		echo "The zipcode $ZIP is in the city of $city "
	fi
fi




