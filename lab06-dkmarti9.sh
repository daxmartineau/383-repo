#Module 6 Lab

#NBA Daily Scores

#!/bin/bash

#This code will search for a day and give the preformances of every nba player who played. 
#They will be ranked by best fantasy point preformers. This lumps all their stats into one score to see
#which players had the most overall impact on the court.

#First Half read and error checking
####################################################################################################################

#Prompt the user to insert a date in month day and year format and a number of players they wish to see.
#use a read - p statement to prompt the user for a numerical month, numerical day and numerical year value.
#use a second read - p to prompt the user to put in a numerical value for the number of players they wish to see.
echo "NBA Daily Scores"
read -p "Select game day (mm dd yyyy): " MONTH DAY YEAR
read -p "How many of the top Fantasy Point scorers do you wish to see: " numOfPlayers


int=^[0-9]+$

#Check to make sure the user put in a numerical value for the number of players they wish to see.
#compare the numOfPlayers value to ^[0-9]+$ which is the regex value for an integer.
#to do regex comparison use =~ between each value.
#If the numOfPlayers is not an integer prompt the user to put in a numerical value and exit the script.
if ! [[ $numOfPlayers =~ $int ]]
then
	echo "The number of players needs to be a numerical value"
	exit
fi
 
#Check to make sure the day value is an integer
#Compare the day value to ^[0-9]+$ in an if statement. ^[0-9]+$ is the regex representation of an integer so
#to use regex comparison put =~ between both values.
#Put ! before the brackets so when the value isnt an integer the if statement will echo a error code
#telling the user to insert a numerical value and exit the script.
if  ! [[ $DAY =~ $int ]]
then
	echo "Day must be a numerical value"
	exit


fi  

#Check to make sure the user put in a day value.
#In an if statement compare -z which checks for empty variables to the day variable.
#If it is empty then echo an error telling the user they must add a day value and exit the script.
if [[ -z $DAY ]]
then
	echo "You must input a day along with a month and year value"
	exit
fi
	
#Check to see if the user input a month value.
#In an if statement compare the month variable to -z. If the variable is empty tell the user
#that they must put a month with a numbered date and exit the script.
if [[ -z $MONTH ]] 
then
	echo "You must include a month with a numbered date and year"
	exit
fi
 
 #Check to make sure the number entered is between 0 and 32.
 #In an if statement check to see in the day variable is greater than or equal to 1 and less than or equal to 31.
 #Use -ge which stands for greater than or equal and -le which stands for less than or equal to.
 #put a ! infront of the brackets to signify looking for the opposite of what is inside.
 #If the day variable is not between 0 and 32, prompt the user to put a proper number and exit the script.
if   ! [[ $DAY -ge 1 && $DAY -le 31 ]]
then
 	echo "The date must be between 1 and 31"
 	exit
fi
 #Check to make sure if the day variable is less than 10 it starts with a 0.
 #In an if statement run an echo command to pipe the value of day variable into a wc- c to count characters.	
 #echo -n removes end of line character to get acurate count.
 #To run a command in an if statement put it in a $()
 #If the number of characters is less than -lt 2 then put a 0 infront of day variable
 #Do variable=0$variable and 0 will now be the starting number.
if [[ $(echo -n $DAY | wc -c) -lt 2 ]]
then
 	DAY=0$DAY
fi
 	
#Check to make sure the user put in a number as the month value.
#in an if statement compare the month variable to ^[0-9]+$ which is the regex representation of an integer.
#use =~ for a regex comparison in an if statement.
#put ! infront so when the mopnth variable isnt an integer a echo statement with an error
#will happen and then an exit will close the script.
if  ! [[ $MONTH =~ $int ]]
then
 	echo "Month must be in numerical form"
 	exit
fi
 
 #Make sure the month value is between 1 and 12.
 #Use an if statement to set the month variable between 1 and 12. Use -ge 1 and -le 12 to say greater than or
 # equal to 1 and less than or equal to 12. Put && between the two statements so both must be true for 
 #the whole statement to return true. Put ! before the brackets to look for cases where it isnt true.
 #If not true echo error telling user to pick number between 1 and 12 and exit script.	
if   ! [[ $MONTH -ge 1 && $MONTH -le 12 ]]
then
 	echo "The month must be between 1 and 12"
 	exit
fi
 	
 #Check to make sure the user put a 0 in front of any month below 10.
 #Use echo -n on the month variable to remove the end of line character and pipe it into a wc -c
 #This will get an acurate number of characters in the month variable. If it is less than 2
 #add a 0 to the value by setting variable=0$variable.
if [[ $(echo -n $MONTH | wc -c) -lt 2 ]]
 then
 	MONTH=0$MONTH
 fi

 
 #Check to see if the user put a year in. 
 #Use an if statement with -z to check if the year variable is empty.
#If the variable is empty prompt the user to inclue a year value and exit the script.

if [[ -z $YEAR ]]
then
 	echo "you must include a year"
else
 
 #If the user did add a year make sure to check if it is an interger.
 #Use an if statement and =~ to use regex to compare the year variable to ^[0-9]+$. This will ensure the 
 #variable is a integer.
 #If it is not an integer then prompt the user to put in a numerical year and exit the script.
 	if ! [[ $YEAR =~ $int ]]
 	then
 		echo "Third field should be a numerical year."
 		exit
 	fi
 #Set a limit on what years the user can input since website only goes back to 2017 and cant predict future scores.
 #In an if statement use -ge and -le to make sure the input date is between 2017 and 2022.
 #if it is not then tell the user to put a number between 2017 and 2022 and exit the script.
 		
 	if ! [[ $YEAR -ge 2017 && $YEAR -le 2022 ]]
 	then
 		echo "Year must be between 2017 and 2022"
 		exit
 	fi
#If the year variable passes the previous tests then set the date variable equal to the year, month and day
#variables with no spaces in between.
#This is the format of the date on the cbssports website.
 	DATE="$YEAR$MONTH$DAY"
fi		





#Second Half: curl and output
####################################################################################################################



#1. Declare two empty arrays to hold the values of the player names and point totals respectivley.
#to delcare an empty array type declare -a arrayName and set it equal to ()
declare -a playerArray=()
declare -a pointArray=()

#2. Collect the names of every player that played on the day the user specified.
#Set a variable equal to the value of the curl command by setting the variable=$(command)
#use a curl command on the cbssports.com website and remove the hard coded date at the end.
#replace the old date with the $DATE variable to search for the webpage on the users requested date
#Pipe that output into a grep which will search for CellPlayerName--long. This should pull up each line containing
#Every players full name. Then pipe that into a cut with a delimiter of > and a field of 7 to remove excess code at the front.
#Pipe this into another cut with -d of <  and -f 1 to remove more excess code on the end.
#pipe that into another cut with -d of " " and a field of 1-2 to get the first and last name. This will
#remove any jrs or II's so only their last name is taken and no extras.
players=$(curl -s -dump https://www.cbssports.com/nba/stats/leaders/live/all-pos/$DATE/ | grep 'CellPlayerName--long' | cut -d '>' -f 7 | cut -d '<' -f 1 | cut -d " " -f 1-2 )

#3. Collect the point values of every player that played on the day the user specified.
#Set a variable equal to the curl command by setting variable=$(command)
#Curl the cbssports.com link and replace the hardcoded date with the $DATE variable.

#Pipe that output into a grep -i which will search of case insensitive values.
#search for </td><td class since that is at the end of every point value in the code.

#The code has many empty spaces so pipe the output into a translate command tr with -s to squeeze the repeating
#space characters into on space.

#There will be a space infront of the values we want so pipe the output into a sed command to edit the code and substitute
#the first space with a backspace. s/ // will substitute a space // represents a backspace and / / represents a space.

#Pipe output into another sed which will delete each line that starts with < or "
#^ represents the start of a line so put that in front of [<"] to represent search for lines starting
#in either < or " and the finish with /d which will delete each instance of that pattern

#now pipe that into a cut with a -d of . and a -f 1 to remove any decimal points that might be in the scores.

#There are still excess words being selected so pip into a grep -o which will select only values that are specified.
#use the regex value [0-9]* to select any number of integers but only integers.
points=$(curl -s -dump https://www.cbssports.com/nba/stats/leaders/live/all-pos/$DATE/ | grep -iP '</td><td class'  | tr -s " " | sed 's/ //' | sed ' /^[<"]/d' | cut -d "." -f 1 | grep -oP '[0-9]*')

#4. Fill the arrays with the values of the players and points variables to seperate each value.
#array=($variable) will put each value of the variable into an index spot in the array.
playerArray=($players)
pointArray=($points)

#5. Since every player has two names multiply the number of players the user requested by 2.
#set variable=$(( variable * 2 )) to do arithmetic
numOfPlayers=$(( $numOfPlayers * 2 ))

#To find out how many total players played on the day the user requested set a variable equal to the length
#of the playerArray. variable=$(echo "${#aray[@]}") will set the variable equal to length of array
#array[@] will expand each value into an argument and then the # will work to count each argument.
pltotal=$(echo "${#playerArray[@]}")

#6. Make sure if the user put in too many players the code wont go over the number of players that played on that day.
#use if statement and compare numOfPlayers to pltotal using -gt. If numOfPlayers is the larger number,
#set numPlayers=$pltotal
if [[ $numOfPlayers -gt $pltotal ]]
then
	numOfPlayers=$pltotal
fi

#7. Check to see if any players actually played on the specified date. If they didnt tell the user no one player.
#If no one played the player array should be empty. use the pltotal variable which is the same value as
#the length of the player array. If it is -eq equal to 0 echo that no one played on that date.	
if [[ $pltotal -eq 0 ]]
then
	echo
	echo "No one played in a game on $MONTH $DAY $YEAR"
	exit
fi

echo
i=0
j=0

#8. Output the names and point totals for the top number of players the user requested.
#Since there are two names that need to be displayed, j will increase by 2 every loop.
#Set j to be less than -lt numOfPlayers since array values start at 0. Will output up to array[numOfPlayers -2]
while [[ $j -lt $numOfPlayers ]]
do

#echo the names of the players by using the j and j+1 values of the playerArray.
# -e allows for the use of \ in an echo statement. \n will help output look nicer.
#need to output i i+2-i+8 values to get every important value
echo -e "${playerArray[j]} ${playerArray[j + 1]} scored ${pointArray[i]} fantasy point(s)
Points: ${pointArray[i + 2]}
Rebounds: ${pointArray[i + 3]}
Assists: ${pointArray[i + 4]}
3pters: ${pointArray[i + 5]}
Steals: ${pointArray[i + 6]}
Blocks: ${pointArray[i + 7]}
TOs: ${pointArray[i + 8]}\n"

#Every player has 9 stats so increase the i variable by 9 each iteration
	i=$(( $i + 9 ))
#Every player has 2 names so increase the j variable by 2 each iteration
	j=$(( $j + 2 ))
done





